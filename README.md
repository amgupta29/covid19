* The objective of the app is to achieve safe social distancing along with the potential to grow into a robust platform to visualise services and available help nearby. 

* Currently the app shows the viewer the anonymous locations of the flu and tested positive COVID-19 people nearby. One can see these locations they should avoid to ensure their safety. The data this app collects is GDPR compliant and there is no way the user can be traced back to their phones.

* The hospitals/state and medical assistance groups can look at the locations where infection is catching up and can take preventive actions. (web console available as a proof of concept)

* Can be leveraged as a platform to mark things on the map nearby, for e.g. people offering help, grocery shops, chemists etc.

* Can be leveraged to set up anonymous communication channels between the hospital/state and among the people who have marked themselves to share information which can be useful to all people listening to it.

* Can warn people if they have been in proximity with anyone who marks themselves with Flu or COVID-19.

* Can integrate with other applications to share data for any representation on the map or for analysis. 
